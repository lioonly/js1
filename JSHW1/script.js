// 1)використання let, var, const або глобальнi змiннi
// 2)функцiя prompt може вивести чи те що ви написали у вiкнi з input чи null при скасуваннi, а функцiя confirm виводе тiльки boolean значення,тобто задається питання на яке можно вiдповiсти тiльки "так" чи "нi"
// 3)неявне перетворення типiв це коли значення одного типу даних автоматично перетворюється на інший тип без вказівок програміста.

//      НАПРИКЛАД

//      let number = 4;
//      let string = "5";
//      let result = number + string;
//      console.log(result); Виведе 45 як string, оскільки 4 автоматично перетворюється в рядок


const name = "Lev";
const admin = name;

console.log(admin);

const days = 3;
const seconds = days * 24 * 60 * 60;
console.log(seconds);

const userInput = prompt("Enter a value:");
console.log(userInput);